# DotA 2 LED GameState Integration

This server runs on my raspberry pi, which has an LED strip connected via pin 18.

It uses the [dota2gsi](https://gitlab.com/avalonparton/dota2gsi) module to trigger lighting events.

I used [this guide](https://gist.github.com/dschep/24aa61672a2092246eaca2824400d37f#installing-python-36-on-raspbian) to install Python 3.6 on Raspbian

## Running

1. Install pre-reqs
    ```shell
    pip3 install -r requirements.txt
    ```

1. Run the script
    ```shell
    python3 dota2led.py
    ```