#!/usr/bin/python3
# Raspberry pi LED strip controlled by DotA GSI
import board
import neopixel
import time

# LED Strip config
NUM_PIXELS = 450
PIXELS = neopixel.NeoPixel(board.D18, NUM_PIXELS, brightness=1.0, pixel_order=neopixel.GRB)

def solid(color):
    """ Sets the LED strip to one solid color """
    PIXELS.fill(color)
    PIXELS.show()

def off():
    """ Turn off the LED strip """
    solid((0, 0, 0))

def flash(color1, color2, times, delay):
    """ Flashes between two colors

    Args:
        color1: A 3-tuple color
        color2: A 3-tuple color
        times:  Number of times change colors
        delay:  Seconds to wait between changes
    """
    for _ in range(0, times):
        solid(color1)
        time.sleep(delay)
        solid(color2)
        time.sleep(delay)