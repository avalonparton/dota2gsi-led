from http.server import BaseHTTPRequestHandler, HTTPServer
import sys
import time
import json
import color

import dota2gsi

health_warning_threshold = 17 # %
default_color = (20, 20, 20)


default_colors = {
    'npc_dota_hero_nevermore': (60,0,0),
    'npc_dota_hero_treant': (105,52,15),
    'npc_dota_hero_venomancer': (0,60,0),
    'npc_dota_hero_zuus': (0, 0, 60)
}

def set_hero(hero_name=None):
    """ Sets the default color for a hero """
    global default_color
    print("setting hero to {}".format(hero_name))
    color = default_colors.get(hero_name)
    if color:
        default_color = color
    else:
        default_color = (20, 20, 20)


def handle_spell(ability):
    """ Shows an animation for supported spells """
    print("Spell cast: {} (level {}) cooldown: {}".format(ability['name'], ability['level'], ability['cooldown']))

    if ability['name'] == 'nevermore_requiem': # SHADOW FIEND R
        steps = 25
        delay = 0.005
        for i in range(0, steps):
            r = int(min(255, 60 + ((i/steps) * 195)))
            color.solid((r, 0, 0))
            time.sleep(delay)
        for i in range(0, steps):
            r = int(min(255, 60 + (((25-i)/25) * 195)))
            color.solid((r, 0, 0))
            time.sleep(delay)

    elif ability['name'] == 'treant_overgrowth': # TREANT R
        for i in range(0, 30):
            g = int(min(255, 30 + ((i/30) * 225)))
            color.solid((0, g, 0))
            time.sleep(0.01)
        for i in range(0, 30):
            g = int(min(255, 30 + (((30-i)/30) * 225)))
            color.solid((0, g, 0))
            time.sleep(0.01)

    elif ability['name'] == 'venomancer_poison_nova': # VENOMANCER R
        steps = 25
        delay = 0.005
        for i in range(0, steps):
            g = int(min(255, 60 + ((i/steps) * 195)))
            color.solid((0, g, 0))
            time.sleep(delay)
        for i in range(0, steps):
            g = int(min(255, 60 + (((steps-i)/steps) * 195)))
            color.solid((0, g, 0))
            time.sleep(delay)

    elif ability['name'] == 'zuus_thundergods_wrath': # ZEUS R
        color.solid((100, 100, 255))
        time.sleep(0.1)
        color.solid((255, 255, 255))
        time.sleep(0.2)
        color.solid((100, 100, 255))
        time.sleep(0.1)

def level_up(level):
    print("level up: {}".format(level))
    steps = 20
    delay = 0.005
    for i in range(0, steps):
        g = int(min(200, 10 + ((i/steps) * 200)))
        color.solid((g, g, 0))
        time.sleep(delay)
    for i in range(0, steps):
        g = int(min(200, 10 + (((steps-i)/steps) * 200)))
        color.solid((g, g, 0))
        time.sleep(delay)

def handle_state(last_state, state):
    hero_name      = state.get('hero', {}).get('name')
    health_percent = state.get('hero', {}).get('health_percent')
    max_health     = state.get('hero', {}).get('max_health')
    alive          = state.get('hero', {}).get('alive')
    game_state     = state.get('map',  {}).get('game_state')

    last_hero_name      = last_state.get('hero', {}).get('name')
    last_health_percent = last_state.get('hero', {}).get('health_percent')
    last_alive          = last_state.get('hero', {}).get('alive')
    last_game_state     = last_state.get('map',  {}).get('game_state')

    # game_state changed
    if game_state != last_game_state:
        print("game_state:", game_state)
        last_game_state = game_state

    # Main menu (no game)
    if game_state is None:
        color.solid(default_color)
        set_hero(None)

    # Game in progress
    elif game_state == 'DOTA_GAMERULES_STATE_GAME_IN_PROGRESS' or game_state == 'DOTA_GAMERULES_STATE_PRE_GAME':
        # hero changed
        if hero_name != last_hero_name:
            set_hero(hero_name)

        # health_percent changed
        if health_percent != last_health_percent:
            if health_percent and last_health_percent:
                current_health = max_health*health_percent/100.0
                print("Health: {}/{} ({}%)".format(current_health, max_health, health_percent))

        # Check health threshold
        if health_percent and health_percent < health_warning_threshold:
            color.solid((255, 0, 0)) # RED
        elif alive is not None and not alive:
            color.solid((10, 0, 0))   # DARK DARK RED
        else:
            color.solid(default_color)

        # Respawned
        if last_alive is not None:
            if not last_alive and alive:
                print("Just respawned")

def main():
    server = dota2gsi.Server(port=56969)
    server.on_ability_cast(handle_spell)
    server.on_exit(color.off)
    server.start()
    color.solid((0, 0, 0))
    print(time.asctime(), '-', "Listener stopped.")

if __name__ == "__main__":
    main()